/**
 * Created by Francis on 5/15/2016.
 */

module.exports = function(io){
    var module = {};

    module.broadcastMessage = function(message) {
        io.sockets.emit('broadcast', message);
    };

    return module;
};
