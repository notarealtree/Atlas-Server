/**
 * Created by Francis on 5/8/2016.
 */
var jwt = require('jsonwebtoken');
var config = require('./config');

var middleware = {};

middleware.tokenParsing = function(req, res, next) {
    if(req.method != 'OPTIONS'){
       var token = req.body.token || req.query.token || req.headers['auth-token'];
       if (token) {
           jwt.verify(token, config.jwt.secret, function(err, decoded) {
               if (err) {
                   return res.json({ success: false, message: 'Failed to authenticate token.' });
               } else {
                   req.decoded = decoded;
                   next();
               }
           });
       } else {
           return res.sendStatus(403).send({
               success: false,
               message: 'No token provided.'
           });
       }
    }else{
       return next();
    }
    // TODO: Parse token on a by route basis.
    return next();
};

middleware.admin = function(req, res, next){
    if(req.decoded.role == 'admin'){
        return next();
    }else{
       res.status(403).json({message: 'Access to the given route is for admin users only'});
    }
};

middleware.ro = function(req, res, next){
    if(req.decoded.role == 'admin' || req.decoded.role == 'ro'){
        return next();
    }else{
       res.status(403).json({message: 'Access to the given route is for RO\'s and admin users only'});
    }
};

middleware.cors = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if(req.method === 'OPTIONS'){
        res.sendStatus(200);
    }else{
        next();
    }
};

module.exports = middleware;