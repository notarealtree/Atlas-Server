/**
 * Created by Francis on 5/7/2016.
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var roleSchema = new Schema({
    name: String
}, {collection: 'roles'});

module.exports = mongoose.model('Role', roleSchema);