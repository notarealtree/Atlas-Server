/**
 * Created by Francis on 5/7/2016.
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var userSchema = new Schema({
    realname : String,
    password : String,
    email    : String,
    role     : String,
    uuid     : String,
    registered: Number
}, {collection: 'users'});

module.exports = mongoose.model('User', userSchema);