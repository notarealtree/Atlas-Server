/**
 * Created by FrancisScreene on 23/05/2016.
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var logEntrySchema = new Schema({
    uuid        : String,
    timestamp   : Number,
    message     : String
}, {collection: 'auditLog'});

module.exports = mongoose.model('LogEntry', logEntrySchema);