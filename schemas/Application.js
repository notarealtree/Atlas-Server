/**
 * Created by Francis on 5/11/2016.
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var appSchema = new Schema({
    name: String,
    timestamp: String,
    comments: String,
    yourCharacter: String,
    source: String,
    whyJoin: String,
    expectation: String,
    shipsLiked: String,
    canFly: String,
    goals: String,
    pvpExperience: String,
    lastCorp: String,
    talkedWith: String,
    dailyActivities: String,
    timezone: String,
    special: String,
    mumble: String,
    tellUs: String,
    realLife: String,
    over18: String,
    appWithMain: String,
    haveMinSkills: String,
    apiKeys: String,
    status: String,
    uuid: String,
    assignee: String,
    outcome: String
}, {collection: 'applications'});

module.exports = mongoose.model('Application', appSchema);