/**
 * Created by Francis on 5/7/2016.
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var interviewSchema = new Schema({
    uuid : String,
    text : String,
    apiKeys: []
}, {collection: 'interview'});

module.exports = mongoose.model('Interview', interviewSchema);