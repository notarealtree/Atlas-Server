/**
 * Created by Francis on 5/10/2016.
 */

var GoogleSpreadsheet = require('google-spreadsheet');
var DbController = require('./dbController');
var UUID = require('uuid');

var credentials = require('../config/credentials.json');
var config = require('../config/config');
var doc = new GoogleSpreadsheet(config.googleSheetKey);
var sheet;
var keysToDelete = ['id', '_links', '_cn6ca', 'save', 'del', '_xml', 'app:edited'];

var applicationController = {};

// TODO: This needs refactoring so we exclude stuff before we try inserting

applicationController.getApplications = function(){
    doc.useServiceAccountAuth(credentials, function(){
        doc.getInfo(function(err, info){
            sheet = info.worksheets[0];
            sheet.getRows({
                offset: 2,
                limit: 500
            }, function( err, rows ){
                rows.forEach(function(row){
                    DbController.storeApplication(preparseRowObject(row));
                });
            });
        });
    });
};

function preparseRowObject(object){
    keysToDelete.forEach(function(key){
        delete object[key];
    });

    Object.keys(config.sheetMapping).forEach(function(key){
        object[config.sheetMapping[key]] = object[key];
        delete object[key];
    });

    object.timestamp = Date.parse(object.timestamp);

    var status = object.status.toLowerCase();
    if(status == 'accepted' || status == 'rejected'){
        object.status = 'complete';
        object.outcome = status;
    }else{
        object.status = 'applied';
        object.outcome = 'incomplete';
    }

    object.assignee = 'Unassigned';

    object.uuid = UUID.v4();

    return object;
}

module.exports = applicationController;