/**
 * Created by Francis on 5/11/2016.
 */
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/atlas');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {console.log('open');});

var Application = require('../schemas/Application');
var LogEntry = require('../schemas/LogEntry');
var User = require('../schemas/User');
var Interview = require('../schemas/Interview');
var Role = require('../schemas/Role');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var uuid = require('uuid');
var slack = require('../modules/slack');
var config = require('../config/config.js');

var dbController = {};

dbController.storeApplication = function(application){
    Application.find({name: application.name, date: application.date}, function(err, results){
        if(results.length == 0){
            var app = new Application(application);
            app.save(function(err){
                //TODO Include date in message
                var text = 'There is a new application from ' + application.name + '.';
                slack.sendMessage({title: 'New Application', text: text}, '#6f5499', true);
            });
        }
    });
};

dbController.getApplications = function(){
    return Application.find({}, function(err, results){
        return results;
    });
};

dbController.setApplicationStatus = function(uuid, status){
    Application.update({uuid: uuid}, {
        status: status
    }, function(err, res){
        console.log('Status', err, res);
    });
};

dbController.setApplicationAssignee = function(uuid, assignee){
    Application.update({uuid: uuid}, {
        assignee: assignee
    }, function(err, res){
        console.log('Assignee', err, res);
    });
};

dbController.setApplicationOutcome = function(uuid, outcome){
    Application.update({uuid: uuid}, {
        outcome: outcome
    }, function(err, res){
        console.log('Outcome', err, res);
    });
};

dbController.newUser = function(email, realname, password, res){
    User.find({email: email}, function(err, results){
        if(results.length == 0){
            console.log('inserting', email, realname, password);
            bcrypt.hash(password, 10, function(err, hash) {
                var user = new User({
                    email: email,
                    realname: realname,
                    password: hash,
                    uuid: uuid.v4(),
                    role: 'unregistered',
                    registered: new Date().getTime()
                });
                user.save(function(err,res){
                    console.log(err, res);
                });
                res.status(200);
                res.json({message: 'User saved'});
                var messageText = realname + ' just registered for an account which needs to be activated.';
                slack.sendMessage({title: 'New User Created', text: messageText}, '#6f5499', true);
            });
        }else{
            res.status(400);
            res.json({message: 'User with email ' + email + ' already exists'});
        }
    });
};

dbController.login = function(email, password, res){
    User.find({email: email}, function(err, results){
        if(results.length > 0){
            if(results[0].role == 'unregistered'){
                res.status(401).json({success: false, message: 'Only registered people may access atlas.'});
                return;
            }
            bcrypt.compare(password, results[0].password, function(err, result){
                var status = -1;
                var response = {success: null};
                if(result){
                    status = 200;
                    response.success = true;
                    response.token = jwt.sign(results[0], config.jwt.secret, {
                        expiresIn: config.jwt.expiry
                    });
                    response.realname = results[0].realname;
                    response.role = results[0].role;
                }else{
                    status = 401;
                    response.success = false;
                }
                res.status(status).json(response);
            });
        }else{
            res.status(401).json({success: false});
        }
    });
};

dbController.insertInterview = function(uuid, text, keys){
    var interview = new Interview({uuid: uuid, text: text, keys: keys});
    interview.save();
};

dbController.roleChange = function(uuid, role, res, socket){
    // TODO: Check if role is valid
    User.find({uuid: uuid}, function(err, results){
        if(results.length == 1){
            User.update({uuid: uuid}, {
                role: role.toLowerCase()
            }, function(err, result){
                if(err == null){
                    res.status(200).json({success: true, message: 'Role successfully changed to ' + role});
                    socket.broadcastMessage({action: 'userUpdate', uuid: uuid, params: {role: role.toLowerCase()}});
                }else{
                    res.status(500).json({success: false, message: err});
                }
            });
        }else{
            res.status(400).json({success: false, message: 'User not found in database'})
        }
    });
};

dbController.addLogEntry = function(uuid, message){
    var entry = new LogEntry({uuid: uuid, timestamp: new Date().getTime(), message: message});
    entry.save();
};

dbController.getRoles = function(res){
    Role.find({}, function(err, results){
        res.status(200).json(results);
    })
};

dbController.getUsers = function(res){
    User.find({}, function(err, results){
        res.status(200).json(results);
    })
};

module.exports = dbController;
