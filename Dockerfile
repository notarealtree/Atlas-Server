FROM ubuntu:latest
MAINTAINER Francis Screene <francis.screene@gmail.com>
RUN apt-get update
RUN apt-get install -y curl \
&& apt-get install -y npm \
&& npm install -g n \
&& n latest
ADD ["./app.js", "./config", "./controllers", "./modules", "./routes", "./schemas", "./sockets", "./package.json", "/opt/atlas/"]
WORKDIR /opt/atlas/app
RUN npm install
EXPOSE 50100
CMD ["node", "app.js"]
