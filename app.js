var path = require('path');
var http = require('http');
var io = require('socket.io');
var logger = require('morgan');
var express = require('express');
var schedule = require('node-schedule');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var users = require('./routes/users');
var config = require('./config/config');
var middleware = require('./config/middleware');
var applicationController = require('./controllers/applicationController');

/**
 * Server Setup
 */
var app = express();
var server = http.createServer(app);
io = io.listen(server);
var socket = require('./sockets/socket')(io);

var applications = require('./routes/applications')(express, socket);
var admin = require('./routes/admin')(socket);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(middleware.cors);
app.use('/applications', middleware.tokenParsing, middleware.ro, applications);
app.use('/admin', middleware.tokenParsing, middleware.admin, admin);
app.use('/users', users);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
    console.log(err.message);
});

module.exports = app;

var j = schedule.scheduleJob(config.cronString, function(){
    applicationController.getApplications();
});

server.listen(8080);