var express = require('express');
var dbController = require('../controllers/dbController');
var router = express.Router();
var middleware = require('../config/middleware');

router.post('/login', function(req, res) {
    var username = req.body.email;
    var password = req.body.password;
    dbController.login(username, password, res);
});

router.post('/register', function(req, res){
    console.log(req.body);
    var username = req.body.email;
    var password = req.body.password;
    var realname = req.body.realname;
    dbController.newUser(username, realname, password, res);
});

module.exports = router;
