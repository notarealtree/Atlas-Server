/**
 * Created by FrancisScreene on 27/05/2016.
 */
var express = require('express');
var dbController = require('../controllers/dbController');

module.exports = function(socket){
    var router = express.Router();

    router.post('/roles', function(req, res){
        dbController.getRoles(res);
    });

    router.post('/roleChange', function(req, res){
        var role = req.body.role;
        var uuid = req.body.uuid;
        dbController.roleChange(uuid, role, res, socket);
    });

    router.post('/users', function(req, res){
        dbController.getUsers(res);
    });


    return router;
};