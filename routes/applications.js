/**
 * Created by Francis on 5/8/2016.
 */

var DbController = require('../controllers/dbController');
var slack = require('../modules/slack');

module.exports = function(express, socket){
    var router = express.Router();

    router.post('/list', function(req, res) {
        DbController.getApplications().then(function(data){
            res.json(data);
        });
    });

    router.post('/setStatus', function(req, res) {
        socket.broadcastMessage({action: 'update', uuid: req.body.uuid, params: {status: req.body.status}});
        DbController.setApplicationStatus(req.body.uuid, req.body.status);
        res.sendStatus(200);
    });

    router.post('/setAssignee', function(req, res) {
        socket.broadcastMessage({action: 'update', uuid: req.body.uuid, params: {assignee: req.body.assignee}});
        DbController.setApplicationAssignee(req.body.uuid, req.body.assignee);
        res.sendStatus(200);
    });

    router.post('/setOutcome', function(req, res) {
        socket.broadcastMessage({action: 'update', uuid: req.body.uuid, params: {outcome: req.body.outcome}});
        DbController.setApplicationOutcome(req.body.uuid, req.body.outcome);
        res.sendStatus(200);
    });

    router.post('/startInterview', function(req, res) {
        slack.sendMessage({title: 'Status update', text: req.body.name + ' has just started interviewing ' + req.body.applicant + '.'}, '#6f5499', false); //TODO This should probably be moved
        res.sendStatus(200);
    });

    router.post('/completeInterview', function(req, res){
        DbController.insertInterview(req.body.uuid, req.body.text, req.body.keys);
        res.sendStatus(200);
    });

    return router;
};
